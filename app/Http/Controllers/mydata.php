<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class mydata extends Controller
{
    //
    public function show( $data )
    {
        if ( trim( $data) == "" )
            return response()->json(['error' => 'data empty'] , 404 );
        else
            return response()->json(['name' => $data] );
    }

}
